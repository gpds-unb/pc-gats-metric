clc;
clear;
genpath('Help Functions')
figure('Visible','off')

img=imread('ref.png');

imwrite(imrotate(img, 90), 'rotated_ref.png');

img = rgb2gray(img);

p = 8;

mapping=getmapping(p,'ri'); 

B = imrotate(img, 90);



% RLBP

RLBP_original = rlbp(img,2, p, mapping, 'i');

imwrite(RLBP_original, 'rlbp_original.png');

[counts_RLBP_original, grayLevels_RLBP_original] = imhist(RLBP_original, 50);
bar(grayLevels_RLBP_original, counts_RLBP_original, 'BarWidth', 1, 'FaceColor', 'r');
xlim([0 255])

saveas(gcf,'histogram_rlbp_original.png')

RLBP_rotated = rlbp(B,2, p, mapping, 'i');

imwrite(RLBP_rotated, 'rlbp_rotated.png');


[counts_RLBP_rotated, grayLevels_RLBP_rotated] = imhist(RLBP_rotated, 50);
bar(grayLevels_RLBP_rotated, counts_RLBP_rotated, 'BarWidth', 1);
xlim([0 255])


saveas(gcf,'histogram_rlbp_rotated.png')

% LBP

LBP_original = lbp(img,2, p, 0, 'i');

imwrite(LBP_original, 'lbp_original.png');


[counts_LBP_original, grayLevels_LBP_original] = imhist(LBP_original, 50);
bar(grayLevels_LBP_original, counts_LBP_original, 'BarWidth', 1, 'FaceColor', 'r');
xlim([0 255])

saveas(gcf,'histogram_lbp_original.png')

LBP_rotated = lbp(B,2, p, 0, 'i');

imwrite(LBP_rotated, 'lbp_rotated.png');


[counts_LBP_rotated, grayLevels_LBP_rotated] = imhist(LBP_rotated, 50);
bar(grayLevels_LBP_rotated, counts_LBP_rotated, 'BarWidth', 1);
xlim([0 255])

saveas(gcf,'histogram_lbp_rotated.png')

JS_RLBP = JSDiv(counts_RLBP_original', counts_RLBP_rotated');
JS_LBP = JSDiv(counts_LBP_original', counts_LBP_rotated');

KL_RLBP = KLDiv(counts_RLBP_original', counts_RLBP_rotated');
KL_LBP = KLDiv(counts_LBP_original', counts_LBP_rotated');

emd_RLBP = pdist2(counts_RLBP_original', counts_RLBP_rotated', 'chisq');
emd_LBP = pdist2(counts_LBP_original', counts_LBP_rotated', 'chisq');

fprintf('%8.5E %8.5E\n', JS_RLBP, JS_LBP)
fprintf('%8.5E %8.5E\n', KL_RLBP, KL_LBP)
fprintf('%8.5E %8.5E\n', emd_RLBP, emd_LBP)