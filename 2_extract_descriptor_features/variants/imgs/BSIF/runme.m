clc;
clear;
img=double(rgb2gray(imread('images/ref.png')));

k = dir('texturefilters/*.mat');
filenames = {k.name}';

for i=1:numel(filenames)
   file = filenames{i};
   [path, filename, extention] = fileparts(file);
   f = strrep(filename,'ICAtextureFilters','');

   infile = sprintf('texturefilters/%s', filename);
   load(infile, 'ICAtextureFilters');
   bsifcodeim = bsif(img,ICAtextureFilters,'im');

   outfile = sprintf('images/bsif_%s.png', f);
   imwrite(uint8(bsifcodeim), outfile)
end



