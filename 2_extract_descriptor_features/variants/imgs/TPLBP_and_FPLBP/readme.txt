MATLAB Code for the Three-Patch LBP (TPLBP) and Four-Patch LBP (FPLBP) global image descriptors

Below please find MATLAB code for producing the TPLBP and FPLBP codes and global image descriptors. Note that this code was not the one used in the experiments reported in the paper, nor are the default parametrs necessarily the same as the ones we used. Please report any bugs or problems to hassner@openu.ac.il.

Type "help TPLBP" or "help FPLBP" for more information on each of these functions.

A typical usage would look something like this:

>> I = imread(...);
>> I = rgb2gray(I);
>> [descI, codeI]=FPLBP(I);
>> descI = descI(:);
