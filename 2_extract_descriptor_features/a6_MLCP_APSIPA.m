clc;
clear;

addpath(genpath('variants/imgs/DRLBP_AND_RLBP'));
addpath(genpath('variants/imgs/LCP'));

metric='MLCP';

M = readtable('/home/pedro/databases/QualityDatabases/PointClouds/reference_APSIPA/apsipa.csv', 'Delimiter', ',');
location_PVS = '/home/pedro/Projects/DeepCompressedPCIQA/PCQA_pc_ttr_folding/1_folding/reference_APSIPA/PVS';
location_REFs = '/home/pedro/Projects/DeepCompressedPCIQA/PCQA_pc_ttr_folding/1_folding/reference_APSIPA/references';


attacks = M.ATTACK;
sampled_scores = M.SCORE;
image_names = M.SIGNAL;
ref_names = M.REF;
locations = M.LOCATION;




for variant={'u2', 'ri', 'riu2'}
    v = variant{1};
    out_table = table();
    for i=1:length(image_names)
        disp(i)
        
        %% Actual PC
        [folder, im_name, extension] = fileparts(image_names{i});
        
        im_name = strcat(location_PVS, '/', im_name, '/results/', im_name, '.png');
        im = imread(im_name);
        s_im = [size(im, 1) size(im,2)];
        
        
        %% REFERENCE PC
        
        [folder, ref_name, extension] = fileparts(ref_names{i});
        
        ref_name = strcat(location_REFs, '/', ref_name, '/results/', ref_name, '.png');
        rim = imread(ref_name);
        s_ref = [size(rim, 1) size(rim,2)];
        
        
        %% All sizes
        im1 = imresize(im, s_ref);
        rim1 = imresize(rim, s_im);
        
        %% impaired features
        fv1 = [];
        fv2 = [];
        for r=[1 2 3]
            for p=[4 8 16]
                mapping=getmapping(p, v);
                feat1 = LCP(double(im), r, p, mapping,'i');
                feat1 = feat1' / sum(feat1);
                fv1 = [fv1 feat1];
                
                feat2 = LCP(double(im1), r, p, mapping,'i');
                feat2 = feat2' / sum(feat2);
                fv2 = [fv2 feat2];
            end
        end
        
        fv = [fv1 fv2];
        
        %% reference features
        rv1 = [];
        rv2 = [];
        for r=[1 2 3]
            for p=[4 8 16]
                mapping=getmapping(p, v);
                rfeat1 = LCP(double(rim), r, p, mapping,'i');
                rfeat1 = rfeat1' / sum(rfeat1);
                rv1 = [rv1 rfeat1];
                
                rfeat2 = LCP(double(rim1), r, p, mapping,'i');
                rfeat2 = rfeat2' / sum(rfeat2);
                rv2 = [rv2 rfeat2];
            end
        end
        
        rv = [rv1 rv2];
        
        
        
        % LINE
        temp = table();
        temp.SCORE = sampled_scores(i);
        temp.ATTACK = cellstr(attacks{i});
        temp.IMG = cellstr(image_names{i});
        temp.LOCATION = cellstr(locations{i});
        temp.REF = cellstr(ref_names{i});
        temp = [temp array2table(fv) array2table(rv)];
        
        out_table = [out_table; temp];
    end
    
    lbptype = sprintf("%s_%s", metric, v);
    out_folder = strcat('features/', lbptype, '/reference_APSIPA/');
    
    if ~exist(out_folder, 'dir')
        mkdir(out_folder)
    end
    
    writetable(out_table, strcat(out_folder, 'features.csv'));
    
end
