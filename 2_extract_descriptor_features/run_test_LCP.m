clc;
clear;

addpath(genpath('variants/imgs/DRLBP_AND_RLBP'));
addpath(genpath('variants/imgs/LCP'));
    
%M = readtable('/home/pedro/QualityDatabases/Images/ESPL_LIVE_HDR_Database/espl.csv');
M = readtable('/home/pedro/QualityDatabases/Images/live2/live2.csv');
%M = readtable('/home/pedro/QualityDatabases/Images/tid2013/tid2013.csv');
%M = readtable('/home/pedro/QualityDatabases/Images/CSIQ/CSIQ.csv');

attacks = M.ATTACK;
sampled_scores = M.SCORE;
locations = M.LOCATION;
image_names = M.SIGNAL;
ref_names = M.REF;



out_table = table();

% Radius and Neighborhood
p = 8;
r = 2;
theta_feature = 90;  

patternMappingriu2 = getmapping(p,'riu2');



for i=1:length(locations)
        disp(i)
        im_name = strcat(locations{i}, '/', image_names{i});
        im = double(imread(im_name));

        fv = LCP(im,r,p, patternMappingriu2, 'i');
        fv = fv' / sum(fv);
        
        temp = table();
        temp.SCORE = sampled_scores(i);
        temp.ATTACK = cellstr(attacks{i});
        temp.IMG = cellstr(image_names{i});
        temp.LOCATION = cellstr(locations{i});
        temp.REF = cellstr(ref_names{i});
        temp = [temp array2table(fv)];
        out_table = [out_table; temp];
end

%writetable(out_table, strcat('[LCP]_CSIQ.csv'));
writetable(out_table, strcat('LCP_FEATURES/[LCP]_LIVE2.csv'));
