clc;
clear;

addpath(genpath('variants/imgs/LBP'));
    
%M = readtable('/home/pedro/QualityDatabases/Images/ESPL_LIVE_HDR_Database/espl.csv');
%M = readtable('/home/pedro/QualityDatabases/Images/live2/live2.csv');
%M = readtable('/home/pedro/QualityDatabases/Images/tid2013/tid2013.csv');
M = readtable('/home/pedro/QualityDatabases/Images/CSIQ/CSIQ.csv');


attacks = M.ATTACK;
sampled_scores = M.SCORE;
locations = M.LOCATION;
image_names = M.SIGNAL;
ref_names = M.REF;



out_table = table();

for i=1:length(locations)
    disp(i)
    im_name = strcat(locations{i}, '/', image_names{i});
    %im = double(rgb2gray(imread(im_name)));
    im = rgb2gray(imread(im_name));
    
    fv = [];
    
    % Radius and Neighborhood
    for r=[1 2 3]
        for p=[4 8 16]
            %mapping=getmapping(p,'u2');
            %mapping=getmapping(p,'ri');
            mapping=getmapping(p,'riu2');
            feat = lbp(im, r, p,mapping,'h');
            fv = [fv feat];
        end
    end
    
    fv = fv / sum(fv);
    
    temp = table();
    temp.SCORE = sampled_scores(i);
    temp.ATTACK = cellstr(attacks{i});
    temp.IMG = cellstr(image_names{i});
    temp.LOCATION = cellstr(locations{i});
    temp.REF = cellstr(ref_names{i});
    temp = [temp array2table(fv)];
    out_table = [out_table; temp];
end
%filename = sprintf('[MLBP]_TID2013__riu2.csv');
filename = sprintf('MLBP_FEATURES/[MLBP]_CSIQ__riu2.csv');
writetable(out_table, filename);
