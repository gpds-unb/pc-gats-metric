clc;
clear;

addpath(genpath('variants/imgs/CLBP'));

%% UnB
M = readtable('/home/pedro/databases/QualityDatabases/PointClouds/QoMEX2019/QoMEX.csv', 'Delimiter', ',');
location_PVS = '/home/pedro/Projects/DeepCompressedPCIQA/PCQA_pc_ttr_folding/1_folding/QoMEX2019';
location_REFs = '/home/pedro/Projects/DeepCompressedPCIQA/PCQA_pc_ttr_folding/1_folding/QoMEX2019';

attacks = M.ATTACK;
sampled_scores = M.SCORE;
image_names = M.SIGNAL;
ref_names = M.REF;
locations = M.LOCATION;


out_table = table();


% Radius and Neighborhood
R=1;
P=8;
patternMappingriu2 = getmapping(P,'riu2');

for i=1:length(image_names)
    disp(i)
    
    %% Actual PC
    [folder, im_name, extension] = fileparts(image_names{i});
    
    im_name = strcat(location_PVS, '/', im_name, '/results/', im_name, '.png');
    
    try
        Gray = double(rgb2gray(imread(im_name)));
        Gray = (Gray-mean(Gray(:)))/std(Gray(:))*20+128;
        
        % CLBP_S feature
        [CLBP_S,CLBP_M,CLBP_C] = clbp(Gray,R,P,patternMappingriu2,'x');
        
        % Generate histogram of CLBP_S/M/C
        CLBP_MCSum = CLBP_M;
        idx = find(CLBP_C);
        CLBP_MCSum(idx) = CLBP_MCSum(idx)+patternMappingriu2.num;
        CLBP_SMC = [CLBP_S(:),CLBP_MCSum(:)];
        Hist3D = hist3(CLBP_SMC,[patternMappingriu2.num,patternMappingriu2.num*2]);
        fv = reshape(Hist3D,1,numel(Hist3D));
        fv = fv / sum(fv);
        
        %% REFERENCE PC
        
        [folder, ref_name, extension] = fileparts(ref_names{i});
        
        ref_name = strcat(location_REFs, '/', ref_name, '/results/', ref_name, '.png');
        
        Gray = double(rgb2gray(imread(ref_name)));
        Gray = (Gray-mean(Gray(:)))/std(Gray(:))*20+128;
        
        % CLBP_S feature
        [CLBP_S,CLBP_M,CLBP_C] = clbp(Gray,R,P,patternMappingriu2,'x');
        
        % Generate histogram of CLBP_S/M/C
        CLBP_MCSum = CLBP_M;
        idx = find(CLBP_C);
        CLBP_MCSum(idx) = CLBP_MCSum(idx)+patternMappingriu2.num;
        CLBP_SMC = [CLBP_S(:),CLBP_MCSum(:)];
        Hist3D = hist3(CLBP_SMC,[patternMappingriu2.num,patternMappingriu2.num*2]);
        rv = reshape(Hist3D,1,numel(Hist3D));
        rv = rv / sum(rv);
        
        
        % LINE
        temp = table();
        temp.SCORE = sampled_scores(i);
        temp.ATTACK = cellstr(attacks{i});
        temp.IMG = cellstr(image_names{i});
        temp.LOCATION = cellstr(locations{i});
        temp.REF = cellstr(ref_names{i});
        temp = [temp array2table(fv) array2table(rv)];
        
        out_table = [out_table; temp];
    catch
        warning(strcat(im_name, 'does not exists. '));
    end
end

out_folder = 'features/CLBP/QoMEX2019/';

if ~exist(out_folder, 'dir')
    mkdir(out_folder)
end

writetable(out_table, strcat(out_folder, 'features.csv'));
