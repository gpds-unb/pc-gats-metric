clc;
clear;

addpath(genpath('variants/imgs/CLBP'));

%% UnB    
M = readtable('/home/pedro/databases/QualityDatabases/PointClouds/reference_APSIPA/apsipa.csv', 'Delimiter', ',');
location_PVS = '/home/pedro/Projects/DeepCompressedPCIQA/PCQA_pc_ttr_folding/1_folding/reference_APSIPA/PVS';
location_REFs = '/home/pedro/Projects/DeepCompressedPCIQA/PCQA_pc_ttr_folding/1_folding/reference_APSIPA/references';

attacks = M.ATTACK;
sampled_scores = M.SCORE;
image_names = M.SIGNAL;
ref_names = M.REF;
locations = M.LOCATION;


out_table = table();


% Radius and Neighborhood
R=2;
P=16;
patternMappingriu2 = getmapping(P,'riu2');

for i=1:length(image_names)
        disp(i)
        
        %% Actual PC
        [~, im_name, ~] = fileparts(image_names{i});
 
        im_name = strcat(location_PVS, '/', im_name, '/results/', im_name, '.png');

        Gray = double(rgb2gray(imread(im_name)));
        Gray = (Gray-mean(Gray(:)))/std(Gray(:))*20+128;
        s_gray = size(Gray);

        
         %% REFERENCE PC

        [~, ref_name, ~] = fileparts(ref_names{i});
 
        ref_name = strcat(location_REFs, '/', ref_name, '/results/', ref_name, '.png');

        Gray_ref = double(rgb2gray(imread(ref_name)));
        Gray_ref = (Gray_ref-mean(Gray_ref(:)))/std(Gray_ref(:))*20+128;
        s_gray_ref = size(Gray_ref);
        
        Gray_1 = imresize(Gray, s_gray_ref);
        Gray_ref_1 = imresize(Gray_ref, s_gray);
        
       
        % CLBP_S feature normal
        [CLBP_S,CLBP_M,CLBP_C] = clbp(Gray,R,P,patternMappingriu2,'x');

        % Generate histogram of CLBP_S/M/C
        CLBP_MCSum = CLBP_M;
        idx = find(CLBP_C);
        CLBP_MCSum(idx) = CLBP_MCSum(idx)+patternMappingriu2.num;
        CLBP_SMC = [CLBP_S(:),CLBP_MCSum(:)];
        Hist3D = hist3(CLBP_SMC,[patternMappingriu2.num,patternMappingriu2.num*2]);
        fv1 = reshape(Hist3D,1,numel(Hist3D));
        fv1 = fv1 / sum(fv1);


        % CLBP_S feature reshaped
        [CLBP_S,CLBP_M,CLBP_C] = clbp(Gray_1,R,P,patternMappingriu2,'x');

        % Generate histogram of CLBP_S/M/C
        CLBP_MCSum = CLBP_M;
        idx = find(CLBP_C);
        CLBP_MCSum(idx) = CLBP_MCSum(idx)+patternMappingriu2.num;
        CLBP_SMC = [CLBP_S(:),CLBP_MCSum(:)];
        Hist3D = hist3(CLBP_SMC,[patternMappingriu2.num,patternMappingriu2.num*2]);
        fv2 = reshape(Hist3D,1,numel(Hist3D));
        fv2 = fv2 / sum(fv2);

        fv = [fv1 fv2];

        
        % CLBP_S feature reference
        [CLBP_S,CLBP_M,CLBP_C] = clbp(Gray_ref,R,P,patternMappingriu2,'x');

        % Generate histogram of CLBP_S/M/C
        CLBP_MCSum = CLBP_M;
        idx = find(CLBP_C);
        CLBP_MCSum(idx) = CLBP_MCSum(idx)+patternMappingriu2.num;
        CLBP_SMC = [CLBP_S(:),CLBP_MCSum(:)];
        Hist3D = hist3(CLBP_SMC,[patternMappingriu2.num,patternMappingriu2.num*2]);
        rv1 = reshape(Hist3D,1,numel(Hist3D));
        rv1 = rv1 / sum(rv1);

        % CLBP_S feature reference interpolado
        [CLBP_S,CLBP_M,CLBP_C] = clbp(Gray_ref_1, R,P,patternMappingriu2,'x');

        % Generate histogram of CLBP_S/M/C
        CLBP_MCSum = CLBP_M;
        idx = find(CLBP_C);
        CLBP_MCSum(idx) = CLBP_MCSum(idx)+patternMappingriu2.num;
        CLBP_SMC = [CLBP_S(:),CLBP_MCSum(:)];
        Hist3D = hist3(CLBP_SMC,[patternMappingriu2.num,patternMappingriu2.num*2]);
        rv2 = reshape(Hist3D,1,numel(Hist3D));
        rv2 = rv2 / sum(rv2);
        
        rv = [rv1 rv2];
       
        % LINE
        temp = table();
        temp.SCORE = sampled_scores(i);
        temp.ATTACK = cellstr(attacks{i});
        temp.IMG = cellstr(image_names{i});
        temp.LOCATION = cellstr(locations{i});
        temp.REF = cellstr(ref_names{i});
        temp = [temp array2table(fv) array2table(rv)];

        out_table = [out_table; temp];
end

out_folder = 'features/CLBP/reference_APSIPA/';

if ~exist(out_folder, 'dir')
    mkdir(out_folder)
end

writetable(out_table, strcat(out_folder, 'features.csv'));
