clc;
clear;

addpath(genpath('variants/imgs/OC-LBP'));


metric='OCLBP';

%% UnB    
M = readtable('/home/pedro/databases/QualityDatabases/PointClouds/UnB/UnB_PC_IMGS.csv', 'Delimiter', ',');
location_PVS = '/home/pedro/Projects/DeepCompressedPCIQA/PCQA_pc_ttr_folding/1_folding/UnB/testing/';
location_REFs = '/home/pedro/Projects/DeepCompressedPCIQA/PCQA_pc_ttr_folding/1_folding/UnB/testing/';

attacks = M.ATTACK;
sampled_scores = M.SCORE;
image_names = M.SIGNAL;
ref_names = M.REF;
locations = M.LOCATION;


out_table = table();


% Radius and Neighborhood
R=2;
P=16;


for i=1:length(image_names)
        disp(i)
        
        %% Actual PC
        [folder, im_name, extension] = fileparts(image_names{i});
 
        im_name = strcat(location_PVS, '/', im_name, '/results/', im_name, '.png');

        im = imread(im_name);
        s_im = [size(im, 1) size(im,2)];

        
        %% REFERENCE PC

        [folder, ref_name, extension] = fileparts(ref_names{i});
 
        ref_name = strcat(location_REFs, '/', ref_name, '/results/', ref_name, '.png');

        rim = imread(ref_name);
        s_ref = [size(rim, 1) size(rim,2)];
        
        
        %% All sizes
        im1 = imresize(im, s_ref);
        rim1 = imresize(rim, s_im);
        
        
        maps = oppositeColorLBP(im);

        fv1 = [];
        
        nOCLBP=size(maps, 3);
        for j=1:nOCLBP
            map = double(maps(:,:,j));
            hist = histnorm(map(:), 255);
            fv1 = [fv1 hist];
        end
        
        
        maps1 = oppositeColorLBP(im1);

        fv2 = [];
        
        nOCLBP=size(maps1, 3);
        for j=1:nOCLBP
            map = double(maps1(:,:,j));
            hist = histnorm(map(:), 255);
            fv2 = [fv2 hist];
        end

        fv = [fv1 fv2];
        
        

        rmaps = oppositeColorLBP(rim);
        rv1 = [];
        
        nOCLBP=size(rmaps, 3);
        for j=1:nOCLBP
            rmap = double(rmaps(:,:,j));
            hist = histnorm(rmap(:), 255);
            rv1 = [rv1 hist];
        end
  
        rmaps1 = oppositeColorLBP(rim1);
        rv2 = [];
        
        nOCLBP=size(rmaps1, 3);
        for j=1:nOCLBP
            rmap1 = double(rmaps(:,:,j));
            hist = histnorm(rmap1(:), 255);
            rv2 = [rv2 hist];
        end
        
        rv = [rv1 rv2];
       
        % LINE
        temp = table();
        temp.SCORE = sampled_scores(i);
        temp.ATTACK = cellstr(attacks{i});
        temp.IMG = cellstr(image_names{i});
        temp.LOCATION = cellstr(locations{i});
        temp.REF = cellstr(ref_names{i});
        temp = [temp array2table(fv) array2table(rv)];

        out_table = [out_table; temp];
end

out_folder = strcat('features/', metric, '/UnB/');

if ~exist(out_folder, 'dir')
    mkdir(out_folder)
end

writetable(out_table, strcat(out_folder, 'features.csv'));
