clc;
clear;

addpath(genpath('variants/imgs/LPQ'));
    
%M = readtable('/home/pedro/QualityDatabases/Images/ESPL_LIVE_HDR_Database/espl.csv');
M = readtable('/home/pedro/QualityDatabases/Images/tid2013/tid2013.csv');
%M = readtable('/home/pedro/QualityDatabases/Images/live2/live2.csv');
%M = readtable('/home/pedro/QualityDatabases/Images/CSIQ/CSIQ.csv');

attacks = M.ATTACK;
sampled_scores = M.SCORE;
locations = M.LOCATION;
image_names = M.SIGNAL;
ref_names = M.REF;



out_table = table();

for i=1:length(locations)
        disp(i)
        im_name = strcat(locations{i}, '/', image_names{i});
        %im = double(rgb2gray(imread(im_name)));
        im = rgb2gray(imread(im_name));

        %fv = lpq(im, 3, 0, 1, 'nh'); % STFT with uniform window (corresponds to basic version of LPQ)
        %fv = lpq(im, 3, 0, 2, 'nh'); % STFT with Gaussian window.
        %fv = lpq(im, 3, 0, 3, 'nh'); % Gaussian derivative quadrature filter pair.

        %fv = [lpq(im, 3, 0, 1, 'nh') lpq(im, 3, 0, 2, 'nh')]; % STFT with uniform window + STFT with Gaussian window
        %fv = [lpq(im, 3, 0, 1, 'nh') lpq(im, 3, 0, 3, 'nh')]; % STFT with uniform window + Gaussian derivative quadrature filter pair
        
        %fv = [lpq(im, 3, 0, 2, 'nh') lpq(im, 3, 0, 3, 'nh')]; % STFT with Gaussian window + Gaussian derivative quadrature filter pair
        
        fv = [lpq(im, 3, 0, 1, 'nh') lpq(im, 3, 0, 2, 'nh') lpq(im, 3, 0, 3, 'nh')]; %ALL
        
        temp = table();
        temp.SCORE = sampled_scores(i);
        temp.ATTACK = cellstr(attacks{i});
        temp.IMG = cellstr(image_names{i});
        temp.LOCATION = cellstr(locations{i});
        temp.REF = cellstr(ref_names{i});
        temp = [temp array2table(fv)];
        out_table = [out_table; temp];
end
%filename = sprintf('[LPQ]_ESPL__GaussianDerivativeQuadrature.csv');
%filename = sprintf('LPQ_FEATURES/[LPQ]_LIVE2__STFTUniformWindow.csv');
%filename = sprintf('LPQ_FEATURES/[LPQ]_LIVE2__STFTGaussianWindow.csv');
%filename = sprintf('LPQ_FEATURES/[LPQ]_LIVE2__GaussianDerivativeQuadrature.csv');
%filename = sprintf('LPQ_FEATURES/[LPQ]_LIVE2__STFTUniformWindowPlusSTFTGaussianWindow.csv');
%filename = sprintf('LPQ_FEATURES/[LPQ]_LIVE2__GaussianDerivativeQuadraturePlusSTFTUniformWindow.csv');
%filename = sprintf('LPQ_FEATURES/[LPQ]_LIVE2__GaussianDerivativeQuadraturePlusSTFTGaussianWindow.csv');
%filename = sprintf('LPQ_FEATURES/[LPQ]_LIVE2__GaussianDerivativeQuadraturePlusSTFTGaussianWindowPlusSTFTUniformWindow.csv');

%filename = sprintf('LPQ_FEATURES/[LPQ]_CSIQ__STFTUniformWindowPlusSTFTGaussianWindow.csv');
%filename = sprintf('LPQ_FEATURES/[LPQ]_CSIQ__GaussianDerivativeQuadraturePlusSTFTUniformWindow.csv');
%filename = sprintf('LPQ_FEATURES/[LPQ]_CSIQ__GaussianDerivativeQuadraturePlusSTFTGaussianWindow.csv');
%filename = sprintf('LPQ_FEATURES/[LPQ]_CSIQ__GaussianDerivativeQuadraturePlusSTFTGaussianWindowPlusSTFTUniformWindow.csv');

%filename = sprintf('LPQ_FEATURES/[LPQ]_TID2013__STFTUniformWindowPlusSTFTGaussianWindow.csv');
%filename = sprintf('LPQ_FEATURES/[LPQ]_TID2013__GaussianDerivativeQuadraturePlusSTFTUniformWindow.csv');
%filename = sprintf('LPQ_FEATURES/[LPQ]_TID2013__GaussianDerivativeQuadraturePlusSTFTGaussianWindow.csv');
filename = sprintf('LPQ_FEATURES/[LPQ]_TID2013__GaussianDerivativeQuadraturePlusSTFTGaussianWindowPlusSTFTUniformWindow.csv');

writetable(out_table, filename);