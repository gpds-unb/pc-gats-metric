clc;
clear;

addpath(genpath('variants/imgs/LPQ'));

metric='MLPQ';

%% UnB
M = readtable('/home/pedro/databases/QualityDatabases/PointClouds/UnB/UnB_PC_IMGS.csv', 'Delimiter', ',');
location_PVS = '/home/pedro/Projects/DeepCompressedPCIQA/PCQA_pc_ttr_folding/1_folding/UnB/testing/';
location_REFs = '/home/pedro/Projects/DeepCompressedPCIQA/PCQA_pc_ttr_folding/1_folding/UnB/testing/';


attacks = M.ATTACK;
sampled_scores = M.SCORE;
image_names = M.SIGNAL;
ref_names = M.REF;
locations = M.LOCATION;




out_table = table();
for i=1:length(image_names)
    disp(i)
    
    %% Actual PC
    [folder, im_name, extension] = fileparts(image_names{i});
    
    im_name = strcat(location_PVS, '/', im_name, '/results/', im_name, '.png');
    im = imread(im_name);
    s_im = [size(im, 1) size(im,2)];
    
    
    %% REFERENCE PC
    
    [folder, ref_name, extension] = fileparts(ref_names{i});
    
    ref_name = strcat(location_REFs, '/', ref_name, '/results/', ref_name, '.png');
    rim = imread(ref_name);
    s_ref = [size(rim, 1) size(rim,2)];
    
    
    %% All sizes
    im1 = imresize(im, s_ref);
    rim1 = imresize(rim, s_im);
    
    %% impaired features
    fv1 = [];
    fv2 = [];
    rv1 = [];
    rv2 = [];
    for winSize=[3 5 7 9 11]
        for channel=[1 2 3]
            feat1 = [lpq(im(:,:,channel), winSize, 0, 1, 'nh') lpq(im(:,:,channel), winSize, 0, 2, 'nh') lpq(im(:,:,channel), winSize, 0, 3, 'nh')];
            feat2 = [lpq(im1(:,:,channel), winSize, 0, 1, 'nh') lpq(im1(:,:,channel), winSize, 0, 2, 'nh') lpq(im1(:,:,channel), winSize, 0, 3, 'nh')];
            fv1 = [fv1 feat1];
            fv2 = [fv2 feat2];
            
            rfeat1 = [lpq(rim(:,:,channel), winSize, 0, 1, 'nh') lpq(rim(:,:,channel), winSize, 0, 2, 'nh') lpq(rim(:,:,channel), winSize, 0, 3, 'nh')];
            rfeat2 = [lpq(rim1(:,:,channel), winSize, 0, 1, 'nh') lpq(rim1(:,:,channel), winSize, 0, 2, 'nh') lpq(rim1(:,:,channel), winSize, 0, 3, 'nh')];
            rv1 = [rv1 rfeat1];
            rv2 = [rv2 rfeat2];
        end
    end
    
    fv = [fv1 fv2];
    rv = [rv1 rv2];
    
    
    % LINE
    temp = table();
    temp.SCORE = sampled_scores(i);
    temp.ATTACK = cellstr(attacks{i});
    temp.IMG = cellstr(image_names{i});
    temp.LOCATION = cellstr(locations{i});
    temp.REF = cellstr(ref_names{i});
    temp = [temp array2table(fv) array2table(rv)];
    
    out_table = [out_table; temp];
end

lbptype = sprintf("%s", metric);
out_folder = strcat('features/', lbptype, '/UnB/');

if ~exist(out_folder, 'dir')
    mkdir(out_folder)
end

writetable(out_table, strcat(out_folder, 'features.csv'));


