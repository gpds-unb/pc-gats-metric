clc;
clear;

addpath(genpath('variants/imgs/LBP'));

M = readtable('/home/pedro/databases/QualityDatabases/PointClouds/UnB/UnB_PC_IMGS.csv', 'Delimiter', ',');
location_PVS = '/home/pedro/Projects/DeepCompressedPCIQA/PCQA_pc_ttr_folding/1_folding/UnB/testing/';
location_REFs = '/home/pedro/Projects/DeepCompressedPCIQA/PCQA_pc_ttr_folding/1_folding/UnB/testing/';

attacks = M.ATTACK;
attacks = M.ATTACK;
sampled_scores = M.SCORE;
image_names = M.SIGNAL;
ref_names = M.REF;
locations = M.LOCATION;



% Radius and Neighborhood
for r=[1 2 3]
    for p=[4 8 16]
        for variant={'u2', 'ri', 'riu2'}
            v = variant{1};
            mapping=getmapping(p, v);
            
            out_table = table();
            
            for i=1:length(locations)
                disp(i)
                
                %% Actual PC
                [folder, im_name, extension] = fileparts(image_names{i});
                
                im_name = strcat(location_PVS, '/', im_name, '/results/', im_name, '.png');
                im = rgb2gray(imread(im_name));
                
                fv = lbp(im, r, p,mapping,'h');
                fv = fv / sum(fv);
                
                [folder, ref_name, extension] = fileparts(ref_names{i});
                ref_name = strcat(location_REFs, '/', ref_name, '/results/', ref_name, '.png');
                rim = rgb2gray(imread(ref_name));
                
                rv = lbp(rim, r, p,mapping,'h');
                rv = rv / sum(rv);
                
                % LINE
                temp = table();
                temp.SCORE = sampled_scores(i);
                temp.ATTACK = cellstr(attacks{i});
                temp.IMG = cellstr(image_names{i});
                temp.LOCATION = cellstr(locations{i});
                temp.REF = cellstr(ref_names{i});
                temp = [temp array2table(fv) array2table(rv)];
                
                out_table = [out_table; temp];
            end
            
            lbptype = sprintf("LBP_%d_%d_%s", r, p, v);
            out_folder = strcat('features/', lbptype, '/UnB/');
            
            if ~exist(out_folder, 'dir')
                mkdir(out_folder);
            end
            
            writetable(out_table, strcat(out_folder, 'features.csv'));
        end
    end
end