clc;
clear;

addpath(genpath('variants/imgs/DRLBP_AND_RLBP'));
    
%M = readtable('/home/pedro/QualityDatabases/Images/ESPL_LIVE_HDR_Database/espl.csv');
M = readtable('/home/pedro/QualityDatabases/Images/live2/live2.csv');
%M = readtable('/home/pedro/QualityDatabases/Images/tid2013/tid2013.csv');
%M = readtable('/home/pedro/QualityDatabases/Images/CSIQ/CSIQ.csv');

attacks = M.ATTACK;
sampled_scores = M.SCORE;
locations = M.LOCATION;
image_names = M.SIGNAL;
ref_names = M.REF;



out_table = table();

% Radius and Neighborhood
p = 16;
r = 3;
theta_feature = 90;  

patternMappingriu2 = getmapping(p,'riu2');



for i=1:length(locations)
        disp(i)
        im_name = strcat(locations{i}, '/', image_names{i});
        im = double(rgb2gray(imread(im_name)));

        fv = rlbp(im,r,p, patternMappingriu2, 'h');
        fv = fv / sum(fv);
        
        temp = table();
        temp.SCORE = sampled_scores(i);
        temp.ATTACK = cellstr(attacks{i});
        temp.IMG = cellstr(image_names{i});
        temp.LOCATION = cellstr(locations{i});
        temp.REF = cellstr(ref_names{i});
        temp = [temp array2table(fv)];
        out_table = [out_table; temp];
end

%writetable(out_table, strcat('[RLBP]_ESPL.csv'));
writetable(out_table, strcat('RLBP_FEATURES/[RLBP]_LIVE2.csv'));
