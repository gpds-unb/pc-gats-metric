#!/bin/bash
export LC_NUMERIC="en_US.UTF-8";

INPUT_DIR=/home/pedro/Projects/DeepCompressedPCIQA/PCQA_pc_ttr_folding/4_single_db_simulations/simulations/

for database in reference_APSIPA UnB;
do
  for metric in PCC SROCC RMSE;
  do
    OUTPUT_DIR=corr
    output=${database}_${metric}.xls
    python script_correlation_regressor_vs_descriptor.py \
      --input ${INPUT_DIR} \
      --dataset ${database} \
      --metric ${metric} \
      --output ${OUTPUT_DIR}/${output}
  done
done