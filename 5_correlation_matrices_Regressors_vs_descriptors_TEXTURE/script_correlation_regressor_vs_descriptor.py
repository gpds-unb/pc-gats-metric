import argparse
import pandas as pd
import numpy as np
from absl import app
from absl.flags import argparse_flags
from scipy.stats import pearsonr, spearmanr, kendalltau
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib.transforms as transforms
import glob
import os
import collections


def PCC(predictions, targets):
    return np.abs(pearsonr(predictions, targets)[0])


def SROCC(predictions, targets):
    return np.abs(spearmanr(predictions, targets)[0])


def RMSE(predictions, targets):
    return np.sqrt(((predictions - targets) ** 2).mean())


def run(args):
    regressors = [os.path.basename(os.path.normpath(p)) for p in glob.glob(
        args.input_dir + '/*/') if os.path.isdir(p)]
    distances = ["d_braycurtis", "d_canberra", "d_chebyshev", "d_cityblock", "d_cosine",
                 "d_euclidean", "d_jensenshannon", "d_wasserstein_distance", "d_energy_distance"]
    temp = {}
    for r in regressors:
        base_path = "{p}/{r}/*/".format(p=args.input_dir, r=r)
        descriptors = [os.path.basename(os.path.normpath(p)) for p in glob.glob(
            base_path) if os.path.isdir(p)]
        for d in descriptors:
            csv_path = "{p}/{r}/{d}/{db}.csv".format(
                p=args.input_dir, r=r, d=d, db=args.dataset)
            df = pd.read_csv(csv_path)
            distances = [c for c in df if c.startswith('d_')]
            for dist in distances:
                if args.metric == "PCC":
                    cor = PCC(df["MOS"], df[dist])
                elif args.metric == "SROCC":
                    cor = SROCC(df["MOS"], df[dist])
                else:
                    cor = RMSE(df["MOS"], df[dist])
                temp[(dist, d, r)] = cor
    df = pd.Series(temp).reset_index()
    df.columns = ['distances', 'descriptor', 'regressor', 'value']
    sheets = {}
    for dist in df['distances'].unique():
        table = df.loc[df['distances'] == dist]
        pivot = table.pivot_table(values='value', index=table.descriptor, columns='regressor', aggfunc='first')
        sheets[dist] = pivot
    print(sheets)
    with pd.ExcelWriter(args.output_file) as writer:
        for sheet_name, table in sheets.items():
            df = table
            df.to_excel(writer, sheet_name=sheet_name)


def parse_args(argv):
    """Parses command line arguments."""
    parser = argparse_flags.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        "--dataset", "-d",
        type=str, dest="dataset",
        help="Database name.")
    parser.add_argument(
        "--metric", "-m",
        type=str, dest="metric",
        help="Metric (PCC, SROCC, or RMSE).")
    parser.add_argument(
        "--input", "-i",
        type=str, dest="input_dir",
        help="Directory base.")
    parser.add_argument(
        "--output", "-o",
        type=str, dest="output_file",
        help="File containing the output plot.")
    args = parser.parse_args(argv[1:])
    return args


def main(args):
    run(args)


if __name__ == "__main__":
    app.run(main, flags_parser=parse_args)
