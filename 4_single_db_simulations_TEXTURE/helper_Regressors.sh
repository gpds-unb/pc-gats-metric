#!/bin/bash
export LC_NUMERIC="en_US.UTF-8";

POSITIONAL=()
while [[ $# -gt 0 ]]
do
    key="$1"
    case $key in
        --regressor)
            regressor="$2"
            shift # past argument
            shift # past value
        ;;
        --default)
            DEFAULT=YES
            shift # past argument
        ;;
        *)    # unknown option
            POSITIONAL+=("$1") # save it in an array for later
            shift # past argument
        ;;
    esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters

BASE_PATH=/home/pedro/Projects/DeepCompressedPCIQA/PCQA_pc_ttr_folding/3_raw_distances/distances

for d in ${BASE_PATH}/*;
do
    descriptor="$(basename $d)"
    for database in UnB reference_APSIPA;
    do
        
        INPUT=${BASE_PATH}/${descriptor}/${database}.csv
        OUTPUT_DIR=simulations/${regressor}/${descriptor}
        mkdir -p ${OUTPUT_DIR}
        output=${OUTPUT_DIR}/${database}.csv
        python script_simulation_singledb_Regressors.py \
        --raw_distances_file ${INPUT} \
        --regressor ${regressor} \
        --output_file ${output}
    done
done