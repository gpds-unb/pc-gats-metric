#!/bin/bash



BASE_PATH=/home/pedro/Projects/DeepCompressedPCIQA/PCQA_pc_ttr_folding/3_raw_distances/distances

for d in ${BASE_PATH}/*;
do
   descriptor="$(basename $d)"
  for database in UnB reference_APSIPA;
  do
    INPUT=${BASE_PATH}/${descriptor}/${database}.csv
    OUTPUT_DIR=simulations/logistic/${descriptor}
    mkdir -p ${OUTPUT_DIR}
    output=${OUTPUT_DIR}/${database}.csv
    python script_simulation_singledb_logistic.py \
       --raw_distances_file ${INPUT} \
        --output_file ${output}
  done
done