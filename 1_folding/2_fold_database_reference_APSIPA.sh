#!/bin/bash

## EDIT ME {
DATASET_PATH=/home/pedro/databases/QualityDatabases/PointClouds/reference_APSIPA
## EDIT ME }

SCRIPT_FOLDERS=pcc_attr_folding/src
DATASET_NAME=reference_APSIPA

TENSORFLOW_DEP_LIB=$(python -c 'import tensorflow as tf; print(tf.sysconfig.get_lib())')
export LD_LIBRARY_PATH="${TENSORFLOW_DEP_LIB}"


PLACE=references
#PLACE=PVS


for fullfile in ${DATASET_PATH}/${PLACE}/*.ply
do
    filename=$(basename -- "$fullfile")
    extension="${filename##*.}"
    filename="${filename%.*}"
    
    OUT_DIR=${DATASET_NAME}/${PLACE}/${filename}
    mkdir -p ${OUT_DIR}

    python ${SCRIPT_FOLDERS}/11_train.py ${fullfile} ${OUT_DIR}/model \
       --max_steps 2000 --model 80_model --input_pipeline 80_input \
       --grid_steps 64,128,1

    python ${SCRIPT_FOLDERS}/20_gen_folding.py ${fullfile} ${OUT_DIR}/results \
        ${OUT_DIR}/model --model 80_model --input_pipeline 80_input \
        --grid_steps auto

    python ${SCRIPT_FOLDERS}/other_21_eval_folding_EXTRACT_PROJECTIONS.py \
      ${OUT_DIR}/results
done