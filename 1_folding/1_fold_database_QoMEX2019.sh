#!/bin/bash

## EDIT ME {
DATASET_PATH=/home/pedro/databases/QualityDatabases/PointClouds/QoMEX2019/contents
## EDIT ME }

TENSORFLOW_DEP_LIB=$(python -c 'import tensorflow as tf; print(tf.sysconfig.get_lib())')
export LD_LIBRARY_PATH="${TENSORFLOW_DEP_LIB}"

SCRIPT_FOLDERS=pcc_attr_folding/src
DATASET_NAME=QoMEX2019


for fullfile in ${DATASET_PATH}/*.ply
do
    fullfile=${DATASET_PATH}/${f}

    filename=$(basename -- "$fullfile")
    extension="${filename##*.}"
    filename="${filename%.*}"
    
    OUT_DIR=${DATASET_NAME}/${filename}
    mkdir -p ${OUT_DIR}

    echo $TENSORFLOW_DEP_LIB

    python ${SCRIPT_FOLDERS}/11_train.py ${fullfile} ${OUT_DIR}/model \
       --max_steps 2000 --model 80_model --input_pipeline 80_input \
       --grid_steps 64,128,1

    python ${SCRIPT_FOLDERS}/20_gen_folding.py ${fullfile} ${OUT_DIR}/results \
        ${OUT_DIR}/model --model 80_model --input_pipeline 80_input \
        --grid_steps auto

    python ${SCRIPT_FOLDERS}/21_eval_folding.py \
       ${OUT_DIR}/results
done
