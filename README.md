# Point Cloud Quality Assessment: Unifying Projection, Geometry, and Texture Similarity

Implementation of temporal pooling methods investigated in the paper "Point Cloud Quality Assessment: Unifying Projection, Geometry, and Texture Similarity".

## Requirements

This project depends of other submodules. Therefore, please clone this repository using the `--recursive` param, i.e.,

```bash
 $ git clone --recursive https://gitlab.com/gpds-unb/pc-gats-metric.git
```

In the following commands, I am using `micromamba` as example, but it also should work with Conda environments. The Python requirements are listed in `requirements.txt` file. To prepare your environment, execute the following command:

```bash
 $ micromamba create -n python36 python=3.6.9
 $ micromamba activate python36
 $ python -m pip install -r requirements.txt
```

Since this project is based on the [Quach's projection method](https://github.com/mauriceqch/pcc_attr_folding), some commands are needed to build the external dependencies. The Quach's code is cloned as a submodule and saved at `pcc_attr_folding`. So, it is recommended to follow the original steps to build this dependency. See [https://github.com/mauriceqch/pcc_attr_folding/blob/master/README.md](https://github.com/mauriceqch/pcc_attr_folding/blob/master/README.md) for more details. In short, the following commands should work:


```bash
 $ cd pcc_attr_folding/src/ops/nn_distance/  
 $ ./compile.sh  
 $ ls *.so  
```

If the commands performed fine, you should see the following files `tf_nndistance2.so  tf_nndistance.so`.


## Execution Steps

### 1. Preprocess datasets

The subdirectory `1_folding` contains the script to preprocess the datasets and create the folded representations of each point cloud. Each script in `1_folding` correlates to the investigated dataset, i.e., `1_fold_database_QoMEX2019.sh`, `2_fold_database_reference_APSIPA.sh`, and `3_fold_database_reference_UnB.sh` corresponds to `QoMEX 2019`, `APSIPA`, and `UnB` datasets, respectively.

Each script in `1_folding` has a variable named `DATASET_PATH`. Please, edit this variable to add the path of the corresponding dataset in your system. After editing it, the script should run after the following commands:

```bash
 $ bash 1_folding/1_fold_database_QoMEX2019.sh  
 $ bash 1_folding/2_fold_database_reference_APSIPA.sh  
 $ bash 1_folding/3_fold_database_reference_UnB.sh  
```

### 2. Extract the features

Open [Matlab](https://www.mathworks.com/products/matlab.html) and run the scripts in the `2_extract_descriptor_features`. The scripts in this directory are named according to the regular expression `a{descriptor-id}_{descriptor-name}_{pc-dataset}.m`. For instance, the script `a1_CLBP_APSIPA.m` corresponds to the code that will extract the features of *APSIPA dataset* using the *Complete Local Binary Pattern* descriptor. In the paper, we reported the results obtained using the MRLBP variant.

### 3. Computed the raw distances

In the paper, we performed an analysis regarding different distances (Euclidian, Bray-Curtis, Wasserstein, Jensen-Shannon, etc). To compute these distances, run the Jupyter notebook saved at `3_raw_distances_TEXTURE/distance_correlations.ipynb`.

### 4. Analysis

The additional results and analysis reported in the paper are present in the directories `4_single_db_simulations_TEXTURE`, `5_correlation_matrices_Regressors_vs_descriptors_TEXTURE`, and `6_geometric_distances`. Check the scripts and Jupyter notebooks present in these directories to see how the results were generated.


## Authors and acknowledgment

This work was supported by the Fundação de Apoio a Pesquisa do Distrito Federal (FAP-DF), by the Coordenação de Aperfeiçoamento de Pessoal de Nível Superior (CAPES), the Conselho Nacional de Desenvolvimento Científico e Tecnológico (CNPq), and by the University of Brasília (UnB). We are grateful to Prof. Luis Cruz, Evangelos Alexiou, Tiago Fonseca, Prof. Ricardo de Queiroz, and Prof. Touradj Ebrahimi for sharing the subjective scores and datasets used in this work.

## License
[![License](https://img.shields.io/badge/license-Unlicense-blue.svg)](LICENSE)

## Citation

If you use this code for your research, please cite our paper.

```
@article{freitas2022point,
  title={Point cloud quality assessment: unifying projection, geometry, and texture similarity},
  author={Freitas, Pedro Garcia and Diniz, Rafael and Farias, Mylene CQ},
  journal={The Visual Computer},
  pages={1--8},
  year={2022},
  publisher={Springer}
}
```