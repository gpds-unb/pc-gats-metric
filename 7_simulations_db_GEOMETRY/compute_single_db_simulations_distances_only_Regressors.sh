#!/bin/bash


for regressor in RandomForestRegressor ExtraTreesRegressor GradientBoostingRegressor BayesianRidge ARDRegression Lars ElasticNet ElasticNetCV Lasso RANSACRegressor KNeighborsRegressor MLPRegressor;
do
    tmux new-session -d -s "simulation-${regressor}" "bash helper_Regressors.sh --regressor ${regressor}"
done