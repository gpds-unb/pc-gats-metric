function [psim] = pointssim(ref, test)
  addpath(genpath('metrics/pointssim/similarity'));
  A = pcread(ref);
  B = pcread(test);
  A.Normal = pcnormals(A);
  B.Normal = pcnormals(B);
    
  psim = pc_ssim(A, B);
end

