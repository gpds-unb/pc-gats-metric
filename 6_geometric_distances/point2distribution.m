function [mseF, mseF_PSNR, mmd, mmd_PSNR, msmd, msmd_PSNR] = point2distribution(ref, test)
pc_error = 'metrics/Point_to_distribution_metric/build/pc_error';
cmd = strcat(pwd, '/', pc_error, ' --fileA=', ref, ' --fileB=', test, ...
    ' --distSize=31');
[status,cmdout] = system(cmd);
focus = extractBetween(cmdout, '3. Final (symmetric)', 'Job done!');
nowhitespaces = strtrim(focus);
nowhitespaces = erase(nowhitespaces, char(10));
splitted = extractNumFromStr(nowhitespaces{1});

mseF = splitted(1);
mseF_PSNR = splitted(2);
mmd = splitted(3);
mmd_PSNR = splitted(4);
msmd = splitted(5);
msmd_PSNR = splitted(6);
end

function numArray = extractNumFromStr(str)
str0=strrep(str,'inf', num2str(intmax('uint16')));
str0=strrep(str0,'p2','');
str1 = regexprep(str0,'[,;=]', ' ');
str2 = regexprep(regexprep(str1,'[^- 0-9.eE(,)/]',''), ' \D* ',' ');
C = regexprep(str2, {'\.\s','\E\s','\e\s','\s\E','\s\e'},' ');
numArray = str2num(C);
end
