function [pl2plMIN, pl2plMAX, pl2plMEAN, pl2plMEDIAN, pl2plRMS, pl2plMSE] = plane2plane(ref, test)
  A = pcread(ref);
  B = pcread(test);
  A.Normal = pcnormals(A);
  B.Normal = pcnormals(B);
  
  [~, ~, pl2plMIN] = angularSimilarity(A, B, 'min');
  [~, ~, pl2plMAX] = angularSimilarity(A, B, 'max');
  [~, ~, pl2plMEAN] = angularSimilarity(A, B, 'mean');
  [~, ~, pl2plMEDIAN] = angularSimilarity(A, B, 'median');
  [~, ~, pl2plRMS] = angularSimilarity(A, B, 'rms');
  [~, ~, pl2plMSE] = angularSimilarity(A, B, 'mse');
end

