clc;
clear;


%% UnB    
M = readtable('/home/pedro/databases/QualityDatabases/PointClouds/UnB/UnB_PC_IMGS.csv', 'Delimiter', ',');

attacks = M.ATTACK;
sampled_scores = M.SCORE;
image_names = M.SIGNAL;
ref_names = M.REF;
locations = M.LOCATION;


out_table = table();


for i=1:length(image_names)
        disp(i)
        
        %% Actual PC
        im_name = strcat(locations{i}, '/', image_names{i});

         %% REFERENCE PC
        ref_name = strcat(locations{i}, '/', ref_names{i});

        [d_mseF, d_mseF_PSNR, d_mmd, d_mmd_PSNR, d_msmd, d_msmd_PSNR] = point2distribution(ref_name, im_name);
        [d_pl2plMIN, d_pl2plMAX, d_pl2plMEAN, d_pl2plMEDIAN, d_pl2plRMS, d_pl2plMSE] = plane2plane(ref_name, im_name);

        % LINE
        temp = table();
        temp.SCORE = sampled_scores(i);
        temp.ATTACK = cellstr(attacks{i});
        temp.IMG = cellstr(image_names{i});
        temp.LOCATION = cellstr(locations{i});
        temp.REF = cellstr(ref_names{i});
        temp = [temp array2table([d_mseF]) array2table([d_mseF_PSNR]) array2table([d_mmd]) array2table([d_mmd_PSNR]) array2table([d_msmd]) array2table([d_msmd_PSNR])];
        temp = [temp array2table([d_pl2plMIN]) array2table([d_pl2plMAX]) array2table([d_pl2plMEAN]) array2table([d_pl2plMEDIAN]) array2table([d_pl2plRMS]) array2table([d_pl2plMSE])];

        out_table = [out_table; temp];
end

out_folder = 'features/UnB/';

if ~exist(out_folder, 'dir')
    mkdir(out_folder)
end

writetable(out_table, strcat(out_folder, 'features.csv'));
