#!/bin/bash

BASE_PATH=/home/pedro/Projects/DeepCompressedPCIQA/PCQA_pc_ttr_folding/2_extract_descriptor_features/features

for d in ${BASE_PATH}/*;
do
  descriptor="$(basename $d)"
  echo ${descriptor}
  for database in UnB reference_APSIPA;
  do
    INPUT=${BASE_PATH}/${descriptor}/${database}/features.csv
    OUTPUT_DIR=distances/${descriptor}
    mkdir -p ${OUTPUT_DIR}
    output=${OUTPUT_DIR}/${database}.csv
    python script_compute_distances.py \
        --feature_file ${INPUT} --output_file ${output}
  done
done
