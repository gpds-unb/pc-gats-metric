import os
import sys
import argparse

import pandas as pd
import numpy as np
from absl import app
from absl.flags import argparse_flags
import scipy.spatial.distance as d
from scipy.stats import wasserstein_distance as EMD
from scipy.stats import energy_distance as energy


distances = [
    d.braycurtis, d.canberra, d.chebyshev, d.cityblock,
    d.cosine, d.euclidean, d.jensenshannon, EMD, energy
]


def run(args):
    df = pd.read_csv(args.feature_file)
    stimuli = len(df.index)
    output = []
    for i in range(stimuli):
        row = df.iloc[[i]]
        PC_columns = [col for col in row if col.startswith('fv')]
        PC_features = row[PC_columns].to_numpy().flatten()
        ref_columns = [c for c in row if c.startswith('rv')]
        reference_features = row[ref_columns].to_numpy().flatten()
        line = {
            'IMG': row["IMG"].tolist()[0],
            'REF': row["REF"].tolist()[0],
            'ATTACK': row["ATTACK"].tolist()[0],
            'SCORE': row["SCORE"].tolist()[0]
        }
        for d in distances:
            d_name = d.__name__
            line["d_" + d_name] = d(reference_features, PC_features)
        output.append(line)
    df_out = pd.DataFrame.from_dict(output)
    df_out.to_csv(args.output_file, index=False)


def parse_args(argv):
    """Parses command line arguments."""
    parser = argparse_flags.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        "--feature_file", "-f",
        type=str, dest="feature_file",
        help="File containing the features.")
    parser.add_argument(
        "--output_file", "-o",
        type=str, dest="output_file",
        help="File containing the processed distances.")
    args = parser.parse_args(argv[1:])
    return args


def main(args):
    run(args)


if __name__ == "__main__":
    app.run(main, flags_parser=parse_args)
