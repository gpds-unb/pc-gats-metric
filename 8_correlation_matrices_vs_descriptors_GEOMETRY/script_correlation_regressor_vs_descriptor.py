import argparse
import pandas as pd
import numpy as np
from absl import app
from absl.flags import argparse_flags
from scipy.stats import pearsonr, spearmanr, kendalltau
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib.transforms as transforms
import glob
import os
import collections


def PCC(predictions, targets):
    return np.abs(pearsonr(predictions, targets)[0])


def SROCC(predictions, targets):
    return np.abs(spearmanr(predictions, targets)[0])


def RMSE(predictions, targets):
    return np.sqrt(((predictions - targets) ** 2).mean())


def run(args):
    regressors = [os.path.basename(os.path.normpath(p)) for p in glob.glob(
        args.input_dir + '/*/') if os.path.isdir(p)]
    temp = {}
    distances = []
    for r in regressors:
        base_path = "{p}/{r}/*/".format(p=args.input_dir, r=r)
        csv_path = "{p}/{r}/{db}.csv".format(
            p=args.input_dir, r=r, db=args.dataset)
        df = pd.read_csv(csv_path)
        distances = [c for c in df if c.startswith('d_')]
        for dist in distances:
            if args.metric == "PCC":
                cor = PCC(df["MOS"], df[dist])
            elif args.metric == "SROCC":
                cor = SROCC(df["MOS"], df[dist])
            else:
                cor = RMSE(df["MOS"], df[dist])
            temp[(dist, r)] = cor
    df = pd.Series(temp).reset_index()
    df.columns = ['distances', 'regressor', 'value']
    sheets = []
    for dist in df['distances'].unique():
        table = df.loc[df['distances'] == dist]
        pivot = table.pivot_table(values='value', index=table.distances, columns='regressor', aggfunc='first')
        sheets.append(pivot)
    print(sheets)
    df_concat = pd.concat(sheets, axis=0, sort=True)
    with pd.ExcelWriter(args.output_file) as writer:
        df_concat.to_excel(writer)


def parse_args(argv):
    """Parses command line arguments."""
    parser = argparse_flags.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        "--dataset", "-d",
        type=str, dest="dataset",
        help="Database name.")
    parser.add_argument(
        "--metric", "-m",
        type=str, dest="metric",
        help="Metric (PCC, SROCC, or RMSE).")
    parser.add_argument(
        "--input", "-i",
        type=str, dest="input_dir",
        help="Directory base.")
    parser.add_argument(
        "--output", "-o",
        type=str, dest="output_file",
        help="File containing the output plot.")
    args = parser.parse_args(argv[1:])
    return args


def main(args):
    run(args)


if __name__ == "__main__":
    app.run(main, flags_parser=parse_args)
